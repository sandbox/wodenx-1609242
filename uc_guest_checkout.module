<?php
/**
 * Lists the config variables whose states are saved and restored
 * before and after guest checkout.
 */
function _uc_guest_checkout_override_variables() {
  $variables = array(
  	'uc_cart_new_account_name', 
    'uc_cart_new_account_password',
    'uc_new_customer_email',
    'uc_new_customer_login',
    'uc_cart_email_validation',
    'uc_new_customer_status_active',
    'uc_cart_new_account_name',
    'uc_cart_new_account_password',
  );
  return $variables;
}

/**
 * Implements hook_menu_alter().
 */
function uc_guest_checkout_menu_alter(&$items) {
  // Take over handling of cart/checkout.
  $items['cart/checkout']['page callback'] = 'uc_guest_checkout_page';
}

/**
 * Implements hook_enable().
 */
function uc_guest_checkout_enable() {
  // Change the default message for existing emails (since no details were entered).
  global $language;
  $lang = $language->language;
  $strings = variable_get('locale_custom_strings_'. $lang, array());
  $replacements = array(
    'An account already exists for your e-mail address. The new account details you entered will be disregarded.' =>
      'An account already exists for your e-mail address. This order will be attached to that account.'
  );
  foreach ($replacements as $orig => $new) {
    if (!isset($strings[$orig])) {
      $strings[$orig] = $new;
    }
  }
  variable_set('locale_custom_strings_' . $lang, $strings);
}

function uc_guest_checkout_uc_get_message_alter(&$messages) {
  if (!empty($messages['completion_new_user'])) {
    $messages['completion_new_user'] = '';
  }
  if (!empty($messages['completion_new_user_logged_in'])) {
    $messages['completion_new_user_logged_in'] = '';
  }
}

/**
 * Implements hook_form_FORM_ID_alter for the checkout settings form.
 */
function uc_guest_checkout_form_uc_cart_checkout_settings_form_alter(&$form, &$form_state) {
  foreach (_uc_guest_checkout_override_variables() as $variable) {
    $form['anonymous'][$variable]['#field_suffix'] = '*';
  }
  $form['anonymous']['uc_guest_checkout_footnote'] = array(
    '#markup' => '*' . t('This setting will be ignored in guest checkout mode.')
  );
  $form['anonymous']['uc_guest_checkout_info'] = array(
    '#type' => 'item',
    '#title' => t('Guest checkout is enabled'),
    '#description' => t('Guest mode allows a more anonymous checkout experience. Guests are not given Drupal accounts; instead their orders are linked either to an existing default account, or with a new blocked account whose details are not passed on to the guest.'),
  );
  $form['anonymous']['uc_guest_checkout_force'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force guest checkout mode'),
    '#description' => t('When checked, all anonymous checkout will be in guest mode.  When unchecked, only users visiting cart/checkout/guest will checkout in guest mode.'),
    '#default_value' => variable_get('uc_guest_checkout_force', TRUE),
  );
  $form['anonymous']['uc_guest_checkout_show'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the guest checkout option page'),
    '#description' => t('When enabled, users will be presented with the option to register or check-out as a guest. When disabled, they will automatically check out as guests.'),
    '#default_value' => variable_get('uc_guest_checkout_show', TRUE),
  );
  $form['anonymous']['guest']['uc_guest_checkout_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Guest checkout account'),
    '#description' => t('Enter the name of a user account to which anonymous orders should be attached. Leave blank to create a new dummy account for each anonymous order.'),
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => variable_get('uc_guest_checkout_account', ''),
  );
  $form['#validate'][] = 'uc_guest_checkout_settings_form_validate';
}

/**
 * Validation callback for the checkout settings form.
 */
function uc_guest_checkout_settings_form_validate($form, &$form_state) {
  $name = isset($form_state['values']['uc_guest_checkout_account']) ? trim($form_state['values']['uc_guest_checkout_account']) : '';
  if (!empty($name)) {
    $uid = db_query('SELECT uid FROM {users} WHERE name = :name', array(':name' => $name))->fetchField();
    if (empty($uid)) {
      form_set_error('uc_guest_checkout_account', t('The specified user name could not be found'));
    }
  }
}

/**
 * Start a guest checkout session.
 */
function _uc_guest_checkout_begin() {
  if (!_uc_guest_checkout_in_progress()) {
    $settings = array();
    foreach (_uc_guest_checkout_override_variables() as $variable) {
      $value = variable_get($variable, NULL);
      if (!is_null($value)) {
        $settings[$variable] = $value;
      }
      variable_set($variable, FALSE);
    }
    $_SESSION['uc_guest_checkout'] = $settings;
  }
}

/**
 * End a guest checkout session.
 */
function _uc_guest_checkout_end() {
  if (_uc_guest_checkout_in_progress()) {
    $settings = $_SESSION['uc_guest_checkout'];
    foreach (_uc_guest_checkout_override_variables()  as $variable) {
      if (isset($settings[$variable])) {
        variable_set($variable, $settings[$variable]);
      }
      else {
        variable_del($variable);
      }
    }
  }
  unset($_SESSION['uc_guest_checkout']);
}

/**
 * Determine whether a guest checkout session is in progress.
 */
function _uc_guest_checkout_in_progress() {
  return !empty($_SESSION['uc_guest_checkout']);
}

/**
 * Replacement page callback for the checkout page.
 */
function uc_guest_checkout_page() {
  global $user;

  // Determine whether or not to show the guest checkout page and/or begin a guest
  // checkout session.
  $use_guest_mode = empty($user->uid) && (variable_get('uc_guest_checkout_force') || arg(2) == 'guest');
  if ($use_guest_mode && !variable_get('uc_guest_checkout_show', TRUE)) {
    _uc_guest_checkout_begin();
  }
  if (!$use_guest_mode || _uc_guest_checkout_in_progress()) {
    module_load_include('inc', 'uc_cart', 'uc_cart.pages');
    return uc_cart_checkout();
  }

  // Checkout restrictions copied from uc_cart_checkout()...
  $items = uc_cart_get_contents();
  if (count($items) == 0 || !variable_get('uc_checkout_enabled', TRUE)) {
    drupal_goto('cart');
  }

  if (($min = variable_get('uc_minimum_subtotal', 0)) > 0) {
    $subtotal = 0;
    if (is_array($items) && count($items) > 0) {
      foreach ($items as $item) {
        $data = module_invoke($item->module, 'uc_cart_display', $item);
        if (!empty($data)) {
          $subtotal += $data['#total'];
        }
      }
    }

    if ($subtotal < $min) {
      drupal_set_message(t('The minimum order subtotal for checkout is @min.', array('@min' => uc_currency_format($min))), 'error');
      drupal_goto('cart');
    }
  }
  
  // Our business logic.
  return array(
    '#theme' => 'uc_guest_checkout',
    'form' => drupal_get_form('uc_guest_checkout_form'),
    'login_form' => drupal_get_form('user_login_block'),
  );
}

/*
function uc_guest_checkout_form_user_login_alter(&$form, &$form_state) {
  if ($_GET['q'] == 'cart/checkout') {
    $form['#submit'][] = 'uc_guest_checkout_user_login_redirect';
  }
}

function uc_guest_checkout_user_login_redirect($form, &$form_state) {
  $form_state['redirect'] = 'cart/checkout';
}
*/

function uc_guest_checkout_theme() {
  return array(
    'uc_guest_checkout' => array(
      'render element' => 'page',
    ),
  );
}

function theme_uc_guest_checkout($variables) {
  $page = $variables['page'];
  $output = '<div><h3>' . t('I don\'t have an account') . '</h3>' 
    . drupal_render($page['form']) . '</div><div><h3>' . t('I have an account') . '</h3><p>' 
    . t('Log in for faster checkout') . '</p>' 
    . drupal_render($page['login_form']) . '</div>';
  return $output;
}


/**
 * Implements hook_uc_order().
 */
function uc_guest_checkout_uc_order($op, &$order) {
  switch ($op) {
    case 'submit':
      // Assign anonymous orders to a default account if one is specified.
      if (!empty($_SESSION['uc_guest_checkout']) && !user_is_logged_in()) {
        if ($name = variable_get('uc_guest_checkout_account', FALSE)) {
          $uid = db_query('SELECT uid FROM {users} WHERE name = :name', array(':name' => $name))->fetchField();
          $order->uid = $uid;
          unset($order->data['new_user']);
          db_update('uc_orders')
          ->condition('order_id', $order->order_id)
          ->fields(array(
            'uid' => $order->uid,
            'data' => serialize($order->data),
          ))
          ->execute();
        }
      } 
      return;
  }
}

/**
 * Implements hook_uc_checkout_complete().
 */
function uc_guest_checkout_uc_checkout_complete($order, $account) {
  if (!empty($_SESSION['uc_guest_checkout'])  && !user_is_logged_in()) {
    switch ($order->data['complete_sale']) {
      case 'new_user':
        // For guest checkout, we replace the ubercart generated username and email
        // for a new user with one that is more anonymous.
        $name = uniqid('anon-', TRUE);
        $mail = $name . '.' . uc_store_email();
        db_query('UPDATE {users} SET name = "%s", mail = "%s" WHERE uid = %d', $name, $mail, $account->uid);
        break;
      case 'logged_in':
        // If the order is marked logged in but anonymous, this means that 
        // we must have set the uid to the default account, but still treat
        // this as a new user.
        $order->data['complete_sale'] = 'new_user';
        break;
    }
  }
  _uc_guest_checkout_end();
}

/**
 * Form builder for the guest checkout form.
 */
function uc_guest_checkout_form($form_state) {
  $form = array();
  $form['guest'] = array(
    '#type' => 'item',
    '#value' => t('You do not need an account to checkout'),
    'button' => array(
      '#type' => 'submit',
      '#value' => t('Checkout as a guest'),
      '#submit' => array('uc_guest_checkout_submit_guest'),
    ),
  );
  $form['register'] = array(
    '#type' => 'item',
    '#value' => t('You can create an account for faster checkout next time, and to access your online order history.'),
    'button' => array(
      '#type' => 'submit',
      '#value' => t('Create an account'),
      '#submit' => array('uc_guest_checkout_submit_register'),
    ),
  );
  return $form;
}

function uc_guest_checkout_submit_register($form, &$form_state) {
  $form_state['redirect'] = array('user/register', array('destination' => 'cart/checkout'));
}

function uc_guest_checkout_submit_guest($form, &$form_state) {
  _uc_guest_checkout_begin();
}


/**
 * Implements hook_form_FORM_ID_alter for the uc_cart_checkout_form.
 */
function uc_guest_checkout_form_uc_cart_checkout_form_alter(&$form, &$form_id) {
  global $user;
  if (_uc_guest_checkout_in_progress()) {
    unset($form['panes']['customer']['new_account']);
  }
}

/**
 * Implements hook_user_login().
 */
function uc_guest_checkout_user_login() {
  _uc_guest_checkout_end();
}

//frodo was here